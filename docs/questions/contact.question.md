---
title: How to contact the developers of Pyxel?
---
If you found a bug or want to suggest a new feature, you can create an [issue on Gitlab](https://gitlab.com/esa/pyxel/-/issues).

If you have a question, you can use the Chat on [Gitter](https://gitter.im/pyxel-framework/community) to get help from the Pyxel community.

[Read more](https://esa.gitlab.io/pyxel/doc/stable/tutorials/get_help.html).

If you are using Pyxel on a regular basis and want to [contritbute](http://localhost:52873/references/contributing.html), let us know.

You can always reach us via email: [pyxel@esa.int](mailto:pyxel@esa.int).
