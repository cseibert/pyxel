---
title: How to run Pyxel?
---

Look in the documentation to know [how to run Pyxel](https://esa.gitlab.io/pyxel/doc/stable/tutorials/running.html).