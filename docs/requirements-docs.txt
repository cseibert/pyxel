# Pyxel additional requirements for generating documentation

sphinx~=4.0
sphinx-book-theme
sphinxcontrib-bibtex
sphinx-panels
ipython != 8.1; python_version < '3.9'
ipython>=8.1; python_version >= '3.9'
sphinx-inline-tabs
myst-nb
sphinx-copybutton
faqtory