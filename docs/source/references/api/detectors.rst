.. _detectors_api:

=========
Detectors
=========

.. currentmodule:: pyxel.detectors

Top-level functions
===================

.. autosummary::

    Detector.load
    Detector.save
    Detector.from_hdf5
    Detector.to_hdf5
    Detector.from_asdf
    Detector.to_asdf


Detector
========

.. autoclass:: Detector
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CCD
---
.. autoclass:: CCD
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

CMOS
----

.. autoclass:: CMOS
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

MKID
----

.. autoclass:: MKID
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:

APD
---

.. autoclass:: APD
    :members:
    :inherited-members:
    :undoc-members:
    :show-inheritance:
    :exclude-members:
