.. _inputs_api:

======
Inputs
======

.. currentmodule:: pyxel

load_image
----------
.. autofunction:: load_image

load_table
----------
.. autofunction:: load_table
